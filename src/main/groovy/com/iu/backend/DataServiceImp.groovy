package com.iu.backend

import java.time.ZoneId
import java.time.ZonedDateTime

import javax.annotation.PostConstruct

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import com.fasterxml.jackson.databind.ObjectMapper

@Service
class TaskServiceImp implements TaskService{

	@Autowired
	TaskRepository taskRepository
	
//	@PostConstruct
	public void initial(){
		taskRepository.deleteAll()
	}
	
	@Override
	public Task createTask(TaskInput taskInput) {
		Task task = new Task()
		task.setName(taskInput.getName())
		task.setDescription(taskInput.getDescription())
		task.setStartDate(taskInput.getStartDate())
		task.setDeadline(taskInput.getDeadline())
		task.setTaskCode(taskInput.getTaskCode())
		task.setAssignTo(taskInput.getAssignTo())
//		task.setReporter(taskInput.getReporter())
		task.setProjectCode(taskInput.getProjectCode())
		task.setParentCode(taskInput.getParentCode())
		task.setState(State.PD)
		task.setStatus(Status.OP)
		
		return taskRepository.save(task)
	}

	@Override
	public Task editTask(Object taskInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteTask(Object id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Task approveTask(Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Task rejectTask(Object id) {
		// TODO Auto-generated method stub
		return null;
	}
}

@Service
class ProjectServiceImp implements ProjectService{

	@Autowired
	ProjectRepository projectRepository
	
	@Autowired
	ActivityService activityService
	
//	@PostConstruct
	public void initial(){
		projectRepository.deleteAll()
	}
	
	@Override
	public Project createProject(ProjectInput projectInput) {
		// TODO Auto-generated method stub
		Project project = new Project()
		project.setName(projectInput.getName())
		project.setDescription(projectInput.getDescription())
		project.setProjectCode(projectInput.getProjectCode())
		project.setStartDate(projectInput.getStartDate())
		project.setDeadline(projectInput.getDeadline())
		project.setState(State.PD)
		project.setStatus(Status.OP)
		def activity = new Activity()
		ObjectMapper mapper = new ObjectMapper()
		project = projectRepository.save(project)
		
		
		activity.setProjectCode(project.projectCode)
		activity.setFunction("Create Project")
		activity.setDescription("data: " + mapper.writeValueAsString(project))
		activityService.createActivity(activity)
		return project
	}

	@Override
	public Project editProject(Object projectInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteProject(Object id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Project approveProject(String id) {
		def project = projectRepository.findById(id).get()
		project.setState(State.AP)
		return projectRepository.save(project);
	}

	@Override
	public Project rejectProject(String id) {
		def project = projectRepository.findById(id).get()
		project.setState(State.DC)
		return projectRepository.save(project)
	}
}

@Service
class OfficeServiceImp implements OfficeService{

	@Override
	public Office createOffice(Object officeInput) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Office editOffice(Object officeInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteOffice(Object id) {
		// TODO Auto-generated method stub
		return false;
	}
}

@Service
class UserInfoServiceImp implements UserInfoService{

	@Override
	public UserInfo createUserInfo(Object userInfoInput) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserInfo editUserInfo(Object userInfoInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteUserInfo(Object id) {
		// TODO Auto-generated method stub
		return false;
	}
}

@Service
class ActivityServiceImp implements ActivityService{

	@Autowired
	ActivityRepository activityRepository
		
	@Override
	public Activity createActivity(Activity activity) {
		return activityRepository.save(activity)
	}

	@Override
	public Activity editActivity(Object activityInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteActivity(Object id) {
		// TODO Auto-generated method stub
		return false;
	}
}