package com.iu.backend

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.coxautodev.graphql.tools.GraphQLResolver


@Component
class TaskResolver implements GraphQLResolver<Task> {

	@Autowired
	UserInfoRepository userInfoRepository
	@Autowired
	ProjectRepository projectReposity
	@Autowired
	TaskRepository taskRepository

	UserInfo getAssignee(Task task) {
		userInfoRepository.findById(task.assignTo)
	}

	UserInfo getReportTo(Task task) {
		userInfoRepository.findById(task.reporter)
	}

	Project getProject(Task task) {
		projectReposity.findByProjectCode(task.projectCode)
	}
	
	Task getParent(Task task) {
		taskRepository.findByTaskCode(task.parentCode)
	}
	
	List<Task> getSubTasks(Task task){
		taskRepository.findByParentCode(task.taskCode)
	}
}

@Component
class ProjectResolver implements GraphQLResolver<Project> {

	@Autowired
	UserInfoRepository userInfoRepository

	@Autowired
	TaskRepository taskRepository
	UserInfo getUser(Project project) {
		userInfoRepository.findById(project.createdBy)
	}

	List<Task> getTasks(Project project){
		taskRepository.findByProjectCode(project.getProjectCode())
	}
}

@Component
class UserInfoResolver implements GraphQLResolver<UserInfo> {

	@Autowired
	OfficeRepository officeRepository
	@Autowired
	ProjectRepository projectReposity

	Office getOffice(UserInfo userInfo) {
		officeRepository.findById(userInfo.officeCode)
	}

	List<Project> getProjects(UserInfo userInfo) {
		List<Project> list = new ArrayList()
		for (String id in userInfo.projectCodes) {
			list.add(projectReposity.findById(id).get())
		}
		list
	}
}

@Component
class OfficeResolver implements GraphQLResolver<Office> {
}

@Component
class ActivityResolver implements GraphQLResolver<Activity> {

	@Autowired
	UserInfoRepository userInfoRepository
	@Autowired
	ProjectRepository projectReposity
	@Autowired
	TaskRepository taskRepository

	UserInfo getUser(Activity activity) {
		userInfoRepository.findById(activity.userId)
	}

	Project getProject(Activity activity) {
		projectReposity.findById(activity.projectCode)
	}

	Task getTask(Activity activity) {
		taskRepository.findById(activity.taskCode)
	}
}

@Component
class Query implements GraphQLQueryResolver {

	@Autowired
	UserInfoRepository userInfoRepository
	@Autowired
	ProjectRepository projectReposity
	@Autowired
	TaskRepository taskRepository
	@Autowired
	ActivityRepository activityRepository
	@Autowired
	OfficeRepository officeRepository

	List<Project> getAllProjects() {
		projectReposity.findAll()
	}
	List<Task> getAllTasks() {
		taskRepository.findAll()
	}
	List<Office> getAllOffices() {
		officeRepository.findAll()
	}
	List<UserInfo> getAllUserInfos() {
		userInfoRepository.findAll()
	}
	List<Activity> getAllActivities() {
		activityRepository.findAll()
	}

	Project getProject(id){
		projectReposity.findById(id).get()
	}

	Task getTask(id){
		taskRepository.findById(id).get()
	}
}

@Component
class Mutation implements GraphQLMutationResolver {
	@Autowired
	TaskService taskService
	@Autowired
	ProjectService projectService
	@Autowired
	UserInfoService userInfoService
	@Autowired
	ActivityService activityService
	@Autowired
	OfficeService officeService

	Task createTask(TaskInput taskInput) {
		taskService.createTask(taskInput)
	}

	UserInfo createUserInfo(userInfoInput) {
		userInfoService.createUserInfo(userInfoInput)
	}

	Office createOffice(officeInput) {
		officeService.createOffice(officeInput)
	}

	Project createProject(ProjectInput projectInput) {
		projectService.createProject(projectInput)
	}

	Activity createActivity(activityInput) {
		activityService.createActivity(activityInput)
	}
	
	Project approveProject(String id) {
		projectService.approveProject(id)
	}
	
	Project rejectProject(String id) {
		projectService.rejectProject(id)
	}
}
