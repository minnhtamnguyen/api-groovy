package com.iu.backend

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.mongodb.config.EnableMongoAuditing
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer

import graphql.schema.GraphQLScalarType

@EnableResourceServer
@EnableMongoAuditing
@SpringBootApplication
class ApprovalProcessGrooveApplication {

	static void main(String[] args) {
		SpringApplication.run(ApprovalProcessGrooveApplication, args)
	}
	
}
