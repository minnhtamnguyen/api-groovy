package com.iu.backend

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
interface TaskService {
	Task createTask(TaskInput taskInput)
	Task editTask( taskInput,id)
	boolean deleteTask( id)
	Task approveTask( id)
	Task rejectTask( id)
}

@Service
interface OfficeService {
	Office createOffice( officeInput)
	Office editOffice( officeInput,id)
	boolean deleteOffice( id)
}

@Service
interface UserInfoService {
	UserInfo createUserInfo( userInfoInput)
	UserInfo editUserInfo( userInfoInput,id)
	boolean deleteUserInfo( id)
}

@Service
interface ProjectService {
	Project createProject(ProjectInput projectInput)
	Project editProject( projectInput, id)
	boolean deleteProject(id)
	Project approveProject(String id)
	Project rejectProject(String id)
}

@Service
interface ActivityService {
	Activity createActivity(Activity activity)
	Activity editActivity( activityInput, id)
	boolean deleteActivity(id)
}

